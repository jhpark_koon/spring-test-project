FROM maven:3-jdk-8

ADD target/demo-0.1.jar /test.jar
#ADD start.sh /usr/local/bin/start.sh

EXPOSE 8080
#CMD ["/usr/local/bin/start.sh", "run"]

#WORKDIR /usr/src/app
#
#COPY . /usr/src/app
#RUN mvn package
#
#ENV PORT 8080
#EXPOSE $PORT
#CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
